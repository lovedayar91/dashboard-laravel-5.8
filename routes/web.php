<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Payment Routes
|--------------------------------------------------------------------------
| By : AbdelRahman - at : 2/2020
*/

/********* HyperPay *********/

Route::get('create-form', 'paymentController@createForm')->name('create-form');
Route::get('payment-result', 'paymentController@paymentResult')->name('payment-result');

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
| By : AbdelRahman - at : 2/2020
*/

#change lang
Route::get('change-lang/{lang}', 'mainController@language')->name('admin_language');
#seen contact
Route::post('contact-seen', 'mainController@contact_seen')->name('contact-seen');
/*  Lang middleware */
Route::middleware('lang')->group(function () {
    #login
    Route::get('login', 'mainController@login')->name('admin_login');
    Route::post('login', 'mainController@post_login')->name('admin_post_login');
    #logout
    Route::get('logout', 'mainController@logout')->name('admin_logout');

    /*  Auth middleware */
    Route::middleware('adminAuth')->group(function () {
        #home
        Route::get('/', 'mainController@home')->name('admin_home');

        Route::middleware('hasPermission')->group(function () {
            /******************************************** settingController Start ********************************************/
            Route::get(
                'settings',
                [
                    'uses' => 'settingController@index', 'as' => 'settings', 'title' => 'الإعدادات', 'icon' => '<i class="fa fa-cog"></i>',
                    'child' => [
                        'updatesetting',
                        'updatesocial',
                        'updatelocation',
                        'updateseo',
                    ]
                ]
            );

            #Update setting
            Route::post('update-setting', ['uses' => 'settingController@update', 'as' => 'updatesetting', 'title' => 'تحديث الإعدادات']);
            #Update social
            Route::post('update-social', ['uses' => 'settingController@social', 'as' => 'updatesocial', 'title' => 'تحديث مواقع التواصل']);
            #Update location
            Route::post('update-location', ['uses' => 'settingController@location', 'as' => 'updatelocation', 'title' => 'تحديث الخريطة']);
            #Update seo
            Route::post('update-seo', ['uses' => 'settingController@seo', 'as' => 'updateseo', 'title' => 'تحديث محركات البحث']);

            /********************************************* settingController End *********************************************/

            /******************************************** permissionController Start ********************************************/
            Route::get(
                'permissions',
                [
                    'uses' => 'permissionController@index', 'as' => 'permissions', 'title' => 'الصلاحيات', 'icon' => '<i class="fa fa-cog"></i>',
                    'child' => [
                        'addpagepermission',
                        'addpermission',
                        'editpagepermission',
                        'updatepermission',
                        'deletepermission',
                    ]
                ]
            );

            #Add permission page
            Route::get('add-page-permission', ['uses' => 'permissionController@add', 'as' => 'addpagepermission', 'title' => 'صفحة اضافة صلاحية']);
            #Add permission
            Route::post('add-permission', ['uses' => 'permissionController@store', 'as' => 'addpermission', 'title' => 'اضافة صلاحية']);
            #Edit permission page
            Route::get('edit-page-permission/{role_id}', ['uses' => 'permissionController@edit', 'as' => 'editpagepermission', 'title' => 'صفحة تعديل صلاحية']);
            #Update permission
            Route::post('update-permission', ['uses' => 'permissionController@update', 'as' => 'updatepermission', 'title' => 'تعديل صلاحية']);
            #Delete permission
            Route::post('delete-permission', ['uses' => 'permissionController@delete', 'as' => 'deletepermission', 'title' => 'حذف صلاحية']);

            /********************************************* permissionController End *********************************************/

            /******************************************** adminController Start ********************************************/
            Route::get(
                'admins',
                [
                    'uses' => 'adminController@index', 'as' => 'admins', 'title' => 'المديرين', 'icon' => '<i class="fa fa-user-circle"></i>',
                    'child' => [
                        'addadmin',
                        'updateadmin',
                        'deleteadmin',
                        'deleteadmins',
                    ]
                ]
            );

            #Add admin
            Route::post('add-admin', ['uses' => 'adminController@store', 'as' => 'addadmin', 'title' => 'اضافة مدير']);
            #Update admin
            Route::post('update-admin', ['uses' => 'adminController@update', 'as' => 'updateadmin', 'title' => 'تعديل مدير']);
            #Delete admin
            Route::post('delete-admin', ['uses' => 'adminController@delete', 'as' => 'deleteadmin', 'title' => 'حذف مدير']);
            #Delete admins
            Route::post('delete-admins', ['uses' => 'adminController@delete_all', 'as' => 'deleteadmins', 'title' => 'حذف اكثر من مدير']);

            /********************************************* adminController End *********************************************/

            /******************************************** userController Start ********************************************/
            Route::get(
                'users',
                [
                    'uses' => 'userController@index', 'as' => 'users', 'title' => 'الأعضاء', 'icon' => '<i class="fa fa-users"></i>',
                    'child' => [
                        'adduser',
                        'updateuser',
                        'sendnotifyuser',
                        'deleteuser',
                        'deleteusers',
                        'changestatususer',
                    ]
                ]
            );

            #Add User
            Route::post('add-user', ['uses' => 'userController@store', 'as' => 'adduser', 'title' => 'اضافة عضو']);
            #Update User
            Route::post('update-user', ['uses' => 'userController@update', 'as' => 'updateuser', 'title' => 'تعديل عضو']);
            #Send notify
            Route::post('send-notify-user', ['uses' => 'userController@send_notify', 'as' => 'sendnotifyuser', 'title' => 'أرسال إشعار']);
            #Change User Status
            Route::post('change-user-status', ['uses' => 'userController@change_user_status', 'as' => 'changestatususer', 'title' => 'بتغير حالة عضو']);
            #Delete User
            Route::post('delete-user', ['uses' => 'userController@delete', 'as' => 'deleteuser', 'title' => 'حذف عضو']);
            #Delete Users
            Route::post('delete-users', ['uses' => 'userController@delete_all', 'as' => 'deleteusers', 'title' => 'حذف اكثر من عضو']);

            /********************************************* userController End *********************************************/

            /******************************************** providerController Start ********************************************/
            Route::get(
                'providers',
                [
                    'uses' => 'providerController@index', 'as' => 'providers', 'title' => 'المندوبين', 'icon' => '<i class="fa fa-users"></i>',
                    'child' => [
                        'addprovider',
                        'updateprovider',
                        'sendnotifyprovider',
                        'deleteprovider',
                        'deleteproviders',
                        'changestatusprovider',
                    ]
                ]
            );

            #Add Provider
            Route::post('add-provider', ['uses' => 'providerController@store', 'as' => 'addprovider', 'title' => 'اضافة مندوب']);
            #Update Provider
            Route::post('update-provider', ['uses' => 'providerController@update', 'as' => 'updateprovider', 'title' => 'تعديل مندوب']);
            #Send notify
            Route::post('send-notify-provider', ['uses' => 'providerController@send_notify', 'as' => 'sendnotifyprovider', 'title' => 'أرسال إشعار']);
            #Change Provider Status
            Route::post('change-provider-status', ['uses' => 'providerController@change_provider_status', 'as' => 'changestatusprovider', 'title' => 'بتغير حالة مندوب']);
            #Delete Provider
            Route::post('delete-provider', ['uses' => 'providerController@delete', 'as' => 'deleteprovider', 'title' => 'حذف مندوب']);
            #Delete Providers
            Route::post('delete-providers', ['uses' => 'providerController@delete_all', 'as' => 'deleteproviders', 'title' => 'حذف اكثر من مندوب']);

            /********************************************* providerController End *********************************************/

            /******************************************** pageController Start ********************************************/
            Route::get(
                'pages',
                [
                    'uses' => 'pageController@index', 'as' => 'pages', 'title' => 'الصفحات الاساسية', 'icon' => '<i class="fa fa-cog"></i>',
                    'child' => [
                        'addpage',
                        'updatepage',
                        'deletepage',
                        'deletepages',
                    ]
                ]
            );

            #Add page
            Route::post('add-page', ['uses' => 'pageController@store', 'as' => 'addpage', 'title' => 'اضافة صفحة']);
            #Update page
            Route::post('update-page', ['uses' => 'pageController@update', 'as' => 'updatepage', 'title' => 'تعديل صفحة']);
            #Delete page
            Route::post('delete-page', ['uses' => 'pageController@delete', 'as' => 'deletepage', 'title' => 'حذف صفحة']);
            #Delete pages
            Route::post('delete-pages', ['uses' => 'pageController@delete_all', 'as' => 'deletepages', 'title' => 'حذف اكثر من صفحة']);

            /********************************************* pageController End *********************************************/

            /******************************************** sectionController Start ********************************************/
            Route::get(
                'sections',
                [
                    'uses' => 'sectionController@index', 'as' => 'sections', 'title' => 'الأقسام', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'addsection',
                        'updatesection',
                        'deletesection',
                        'deletesections',
                    ]
                ]
            );

            #Add section
            Route::post('add-section', ['uses' => 'sectionController@store', 'as' => 'addsection', 'title' => 'اضافة قسم']);
            #Update section
            Route::post('update-section', ['uses' => 'sectionController@update', 'as' => 'updatesection', 'title' => 'تعديل قسم']);
            #Delete section
            Route::post('delete-section', ['uses' => 'sectionController@delete', 'as' => 'deletesection', 'title' => 'حذف قسم']);
            #Delete sections
            Route::post('delete-sections', ['uses' => 'sectionController@delete_all', 'as' => 'deletesections', 'title' => 'حذف اكثر من قسم']);

            /********************************************* sectionController End *********************************************/

            /******************************************** serviceController Start ********************************************/
            Route::get(
                'services',
                [
                    'uses' => 'serviceController@index', 'as' => 'services', 'title' => 'الخدمات', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'addservice',
                        'updateservice',
                        'deleteservice',
                        'deleteservices',
                    ]
                ]
            );

            #Add service
            Route::post('add-service', ['uses' => 'serviceController@store', 'as' => 'addservice', 'title' => 'اضافة خدمة']);
            #Update service
            Route::post('update-service', ['uses' => 'serviceController@update', 'as' => 'updateservice', 'title' => 'تعديل خدمة']);
            #Delete service
            Route::post('delete-service', ['uses' => 'serviceController@delete', 'as' => 'deleteservice', 'title' => 'حذف خدمة']);
            #Delete services
            Route::post('delete-services', ['uses' => 'serviceController@delete_all', 'as' => 'deleteservices', 'title' => 'حذف اكثر من خدمة']);

            /********************************************* serviceController End *********************************************/

            /******************************************** countryController Start ********************************************/
            Route::get(
                'countrys',
                [
                    'uses' => 'countryController@index', 'as' => 'countrys', 'title' => 'الدول', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'addcountry',
                        'updatecountry',
                        'deletecountry',
                        'deletecountrys',
                    ]
                ]
            );

            #Add country
            Route::post('add-country', ['uses' => 'countryController@store', 'as' => 'addcountry', 'title' => 'اضافة دولة']);
            #Update country
            Route::post('update-country', ['uses' => 'countryController@update', 'as' => 'updatecountry', 'title' => 'تعديل دولة']);
            #Delete country
            Route::post('delete-country', ['uses' => 'countryController@delete', 'as' => 'deletecountry', 'title' => 'حذف دولة']);
            #Delete countrys
            Route::post('delete-countrys', ['uses' => 'countryController@delete_all', 'as' => 'deletecountrys', 'title' => 'حذف اكثر من دولة']);

            /********************************************* countryController End *********************************************/

            /******************************************** cityController Start ********************************************/
            Route::get(
                'citys',
                [
                    'uses' => 'cityController@index', 'as' => 'citys', 'title' => 'المدن', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'addcity',
                        'updatecity',
                        'deletecity',
                        'deletecitys',
                    ]
                ]
            );

            #Add city
            Route::post('add-city', ['uses' => 'cityController@store', 'as' => 'addcity', 'title' => 'اضافة مدينة']);
            #Update city
            Route::post('update-city', ['uses' => 'cityController@update', 'as' => 'updatecity', 'title' => 'تعديل مدينة']);
            #Delete city
            Route::post('delete-city', ['uses' => 'cityController@delete', 'as' => 'deletecity', 'title' => 'حذف مدينة']);
            #Delete citys
            Route::post('delete-citys', ['uses' => 'cityController@delete_all', 'as' => 'deletecitys', 'title' => 'حذف اكثر من مدينة']);

            /********************************************* cityController End *********************************************/

            /******************************************** neighborhoodController Start ********************************************/
            Route::get(
                'neighborhoods',
                [
                    'uses' => 'neighborhoodController@index', 'as' => 'neighborhoods', 'title' => 'الأحياء', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'addneighborhood',
                        'updateneighborhood',
                        'deleteneighborhood',
                        'deleteneighborhoods',
                    ]
                ]
            );

            #Add neighborhood
            Route::post('add-neighborhood', ['uses' => 'neighborhoodController@store', 'as' => 'addneighborhood', 'title' => 'اضافة حي']);
            #Update neighborhood
            Route::post('update-neighborhood', ['uses' => 'neighborhoodController@update', 'as' => 'updateneighborhood', 'title' => 'تعديل حي']);
            #Delete neighborhood
            Route::post('delete-neighborhood', ['uses' => 'neighborhoodController@delete', 'as' => 'deleteneighborhood', 'title' => 'حذف حي']);
            #Delete neighborhoods
            Route::post('delete-neighborhoods', ['uses' => 'neighborhoodController@delete_all', 'as' => 'deleteneighborhoods', 'title' => 'حذف اكثر من حي']);

            /********************************************* neighborhoodController End *********************************************/

            /******************************************** contactController Start ********************************************/
            Route::get(
                'contacts',
                [
                    'uses' => 'contactController@index', 'as' => 'contacts', 'title' => 'تواصل معنا', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'deletecontact',
                        'deletecontacts',
                    ]
                ]
            );

            #Delete contact
            Route::post('delete-contact', ['uses' => 'contactController@delete', 'as' => 'deletecontact', 'title' => 'حذف رسالة']);
            #Delete contacts
            Route::post('delete-contacts', ['uses' => 'contactController@delete_all', 'as' => 'deletecontacts', 'title' => 'حذف اكثر من رسالة']);

            /********************************************* contactController End *********************************************/

            /******************************************** adminReportController Start ********************************************/
            Route::get(
                'adminReports',
                [
                    'uses' => 'adminReportController@index', 'as' => 'adminReports', 'title' => 'تقارير لوحة التحكم', 'icon' => '<i class="nav-icon fas fa-copy"></i>',
                    'child' => [
                        'deleteadminReport',
                        'deleteadminReports',
                    ]
                ]
            );

            #Delete adminReport
            Route::post('delete-adminReport', ['uses' => 'adminReportController@delete', 'as' => 'deleteadminReport', 'title' => 'حذف تقرير']);
            #Delete adminReports
            Route::post('delete-adminReports', ['uses' => 'adminReportController@delete_all', 'as' => 'deleteadminReports', 'title' => 'حذف اكثر من تقرير']);

            /********************************************* adminReportController End *********************************************/
        });
    });
});
