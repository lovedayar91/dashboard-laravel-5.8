<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class settingController extends Controller
{
    #index
    public function index()
    {
        return view('dashboard.settings');
    }

    #update
    public function update(Request $request)
    {
        if ($request->hasFile('logo')) {
            $logo = Setting::firstOrCreate(['key' => 'logo']);
            $logo->value = upload_image($request->file('logo'), 'public/images/setting');
            $logo->save();
        }

        $site_name = Setting::firstOrCreate(['key' => 'site_name']);
        $site_name->value = $request->site_name;
        $site_name->save();

        $phone = Setting::firstOrCreate(['key' => 'phone']);
        $phone->value = $request->phone;
        $phone->save();

        $mobile = Setting::firstOrCreate(['key' => 'mobile']);
        $mobile->value = $request->mobile;
        $mobile->save();

        $email = Setting::firstOrCreate(['key' => 'email']);
        $email->value = $request->email;
        $email->save();

        $benfit_percentage = Setting::firstOrCreate(['key' => 'benfit_percentage']);
        $benfit_percentage->value = $request->benfit_percentage;
        $benfit_percentage->save();

        $address = Setting::firstOrCreate(['key' => 'address']);
        $address->value = $request->address;
        $address->save();

        $sitelat = Setting::firstOrCreate(['key' => 'lat']);
        $sitelat->value = $request->lat;
        $sitelat->save();

        $sitelng = Setting::firstOrCreate(['key' => 'lng']);
        $sitelng->value = $request->lng;
        $sitelng->save();

        #add adminReport
        admin_report('تحديث الإعدادات');

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }

    #social
    public function social(Request $request)
    {
        $facebook = Setting::firstOrCreate(['key' => 'facebook']);
        $facebook->value = $request->facebook;
        $facebook->save();

        $twitter = Setting::firstOrCreate(['key' => 'twitter']);
        $twitter->value = $request->twitter;
        $twitter->save();

        $instagram = Setting::firstOrCreate(['key' => 'instagram']);
        $instagram->value = $request->instagram;
        $instagram->save();

        $snapchat = Setting::firstOrCreate(['key' => 'snapchat']);
        $snapchat->value = $request->snapchat;
        $snapchat->save();

        #add adminReport
        admin_report('تحديث بيانات مواقع التواصل');

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }

    #update
    public function location(Request $request)
    {
        $sitelat = Setting::firstOrCreate(['key' => 'lat']);
        $sitelat->value = $request->lat;
        $sitelat->save();

        $sitelng = Setting::firstOrCreate(['key' => 'lng']);
        $sitelng->value = $request->lng;
        $sitelng->save();

        #add adminReport
        admin_report('تحديث الخريطة');

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }

    #seo
    public function seo(Request $request)
    {
        $description = Setting::firstOrCreate(['key' => 'description']);
        $description->value = $request->description;
        $description->save();

        $key_words = Setting::firstOrCreate(['key' => 'key_words']);
        $key_words->value = $request->key_words;
        $key_words->save();

        #add adminReport
        admin_report('تحديث بيانات محرك البحث');

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }
}
