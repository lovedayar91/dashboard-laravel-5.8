<?php

namespace App\Http\Controllers;

use App\Role;
use Validator;
use App\User;
use Illuminate\Http\Request;

class adminController extends Controller
{
    #index
    public function index()
    {
        $data = get_users_by('admin', 'asc', 0);
        return view('dashboard.admins', compact('data'));
    }

    #store
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar'     => 'required|image',
            'name'       => 'required|max:255',
            'email'      => 'required|max:255|email|unique:users,email',
            'phone'      => 'required|min:9|max:13|unique:users,phone',
            'role_id'    => 'required|max:255',
            'password'   => 'required|min:6|max:255',
        ]);

        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);

        #store new admin
        $admin = new User;
        $admin->name       = $request->name;
        $admin->phone      = convert_to_english($request->phone);
        $admin->email      = $request->email;
        $admin->role_id    = $request->role_id;
        $admin->password   = bcrypt($request->password);
        $admin->user_type  = 'admin';
        if ($request->hasFile('avatar')) $admin->avatar = upload_image($request->file('avatar'), 'public/images/admins');
        $admin->save();

        #add adminReport
        admin_report('أضافة المدير ' . $request->name);

        #success response
        session()->flash('success', awtTrans('تم الحفظ بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم الحفظ بنجاح')]);
    }

    #update
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar'     => 'nullable|image',
            'name'       => 'required|max:255',
            'phone'      => 'required|min:9|max:13|unique:users,phone,' . $request->id,
            'email'      => 'required|max:255|email|unique:users,email,' . $request->id,
            // 'password'   => 'nullable|min:6|max:255',
            'role_id'    => 'required|max:255',

        ]);

        #error response
        if ($validator->fails())
            return response()->json(['value' => 0, 'msg' => $validator->errors()->first()]);

        #update admin
        $admin = User::whereId($request->id)->first();
        $admin->name     = $request->name;
        $admin->phone    = convert_to_english($request->phone);
        $admin->email    = $request->email;
        $admin->role_id  = $request->role_id;
        if ($request->hasFile('avatar')) $admin->avatar = upload_image($request->file('avatar'), 'public/images/admins');
        $admin->save();

        #add adminReport
        admin_report('تعديل المدير ' . $request->name);

        #success response
        session()->flash('success', awtTrans('تم التعديل بنجاح'));
        return response()->json(['value' => 1, 'msg' => awtTrans('تم التعديل بنجاح')]);
    }

    #delete one
    public function delete(Request $request)
    {
        #get admin
        $admin = User::whereId($request->id)->firstOrFail();
        $name = $admin->name;

        #send FCM

        #delete admin
        $admin->delete();

        #add adminReport
        admin_report('حذف المدير ' . $name);

        #success response
        return back()->with('success', awtTrans('تم الحذف'));
    }

    #delete more than one or all
    public function delete_all(Request $request)
    {
        $type = $request->type;
        #get admins
        if ($type == 'all') $admins = User::where('user_type', 'admin')->get();
        else {
            $ids = $request->admin_ids;
            $first_ids   = ltrim($ids, ',');
            $second_ids  = rtrim($first_ids, ',');
            $admin_ids    = explode(',', $second_ids);
            $admins       = User::whereIn('id', $admin_ids)->get();
        }

        foreach ($admins as $admin) {
            #send FCM

            #delete admin
            $admin->delete();
        }

        #add adminReport
        admin_report('حذف اكتر من مدير');

        #success response
        return back()->with('success', awtTrans('تم الحذف'));
    }
}
