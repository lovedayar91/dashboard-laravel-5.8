-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2020 at 02:18 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_reports`
--

CREATE TABLE `admin_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_reports`
--

INSERT INTO `admin_reports` (`id`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'قام المدير المدير العام بتحديث الإعدادات', '2020-03-13 10:42:05', '2020-03-13 10:42:05'),
(2, 'قام المدير المدير العام بتحديث بيانات مواقع التواصل', '2020-03-13 10:42:33', '2020-03-13 10:42:33'),
(3, 'قام المدير المدير العام بتحديث الخريطة', '2020-03-13 10:43:06', '2020-03-13 10:43:06'),
(4, 'قام المدير المدير العام بتحديث بيانات محرك البحث', '2020-03-13 10:43:24', '2020-03-13 10:43:24'),
(5, 'قام المدير المدير العام بتحديث بيانات محرك البحث', '2020-03-13 10:45:55', '2020-03-13 10:45:55'),
(6, 'قام المدير المدير العام بتحديث الخريطة', '2020-03-13 10:45:59', '2020-03-13 10:45:59'),
(7, 'قام المدير المدير العام بتحديث بيانات مواقع التواصل', '2020-03-13 10:46:04', '2020-03-13 10:46:04'),
(8, 'قام المدير المدير العام بتحديث الإعدادات', '2020-03-13 10:46:07', '2020-03-13 10:46:07'),
(9, 'قام المدير المدير العام بتحديث الخريطة', '2020-03-13 10:47:38', '2020-03-13 10:47:38'),
(10, 'قام المدير المدير العام بتعديل الصلاحية Super Admin', '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(11, 'قام المدير المدير العام بأضافة الصلاحية Admin', '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(12, 'قام المدير المدير العام بأضافة المدير احمد', '2020-03-13 10:49:31', '2020-03-13 10:49:31'),
(13, 'قام المدير المدير العام بأضافة العميل عبدالرحمن', '2020-03-13 10:50:31', '2020-03-13 10:50:31'),
(14, 'قام المدير المدير العام بأضافة العميل محمد', '2020-03-13 10:51:17', '2020-03-13 10:51:17'),
(15, 'قام المدير المدير العام بأضافة العميل يوسف', '2020-03-13 10:51:57', '2020-03-13 10:51:57'),
(16, 'قام المدير المدير العام بأضافة العميل ابراهيم', '2020-03-13 10:53:07', '2020-03-13 10:53:07'),
(17, 'قام المدير المدير العام بأضافة المندوب محمود', '2020-03-13 10:54:01', '2020-03-13 10:54:01'),
(18, 'قام المدير المدير العام بأضافة المندوب سمير', '2020-03-13 10:55:03', '2020-03-13 10:55:03'),
(19, 'قام المدير المدير العام بأضافة المندوب حمدي', '2020-03-13 10:56:01', '2020-03-13 10:56:01'),
(20, 'قام المدير المدير العام بأضافة المندوب عبد الله', '2020-03-13 10:57:23', '2020-03-13 10:57:23'),
(21, 'قام المدير المدير العام بأضافة القسم مطاعم', '2020-03-13 11:01:49', '2020-03-13 11:01:49'),
(22, 'قام المدير المدير العام بأضافة القسم كافيهات', '2020-03-13 11:02:51', '2020-03-13 11:02:51'),
(23, 'قام المدير المدير العام بأضافة الخدمة برجر كينج', '2020-03-13 11:04:37', '2020-03-13 11:04:37'),
(24, 'قام المدير المدير العام بأضافة الخدمة كوستا', '2020-03-13 11:05:15', '2020-03-13 11:05:15'),
(25, 'قام المدير المدير العام بأضافة الخدمة كنتاكي', '2020-03-13 11:05:58', '2020-03-13 11:05:58'),
(26, 'قام المدير المدير العام بأضافة الخدمة ستاربكس', '2020-03-13 11:07:13', '2020-03-13 11:07:13'),
(27, 'قام المدير المدير العام بأضافة الدولة مصر', '2020-03-13 11:13:49', '2020-03-13 11:13:49'),
(28, 'قام المدير المدير العام بأضافة الدولة السعودية', '2020-03-13 11:14:13', '2020-03-13 11:14:13'),
(29, 'قام المدير المدير العام بأضافة المدينة الرياض', '2020-03-13 11:15:06', '2020-03-13 11:15:06'),
(30, 'قام المدير المدير العام بأضافة المدينة القاهرة', '2020-03-13 11:15:54', '2020-03-13 11:15:54'),
(31, 'قام المدير المدير العام بأضافة المدينة العليا', '2020-03-13 11:16:56', '2020-03-13 11:16:56'),
(32, 'قام المدير المدير العام بأضافة المدينة مدينة نصر', '2020-03-13 11:17:12', '2020-03-13 11:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `title_ar`, `title_en`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'الرياض', 'Riyadh', 2, '2020-03-13 11:15:06', '2020-03-13 11:15:06'),
(2, 'القاهرة', 'Cairo', 1, '2020-03-13 11:15:54', '2020-03-13 11:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `title_ar`, `title_en`, `code`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'مصر', 'Egypt', 20, 'EG', '2020-03-13 11:13:49', '2020-03-13 11:13:49'),
(2, 'السعودية', 'Saudi arabia', 966, 'SAR', '2020-03-13 11:14:13', '2020-03-13 11:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_02_03_092946_entrust_setup_tables', 1),
(2, '2014_10_12_000900_create_countries_table', 1),
(3, '2014_10_12_000910_create_cities_table', 1),
(4, '2014_10_12_000920_create_neighborhoods_table', 1),
(5, '2014_10_12_001000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2020_02_10_095308_create_settings_table', 1),
(8, '2020_02_10_101746_create_socials_table', 1),
(9, '2020_02_10_145823_create_admin_reports_table', 1),
(10, '2020_03_04_152050_create_sections_table', 1),
(11, '2020_03_04_152053_create_services_table', 1),
(12, '2020_03_08_105457_create_contacts_table', 1),
(13, '2020_03_08_154000_create_pages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `title_ar`, `title_en`, `country_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'العليا', 'Al Alia', 2, 1, '2020-03-13 11:16:56', '2020-03-13 11:16:56'),
(2, 'مدينة نصر', 'Nasr City', 1, 2, '2020-03-13 11:17:12', '2020-03-13 11:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title_ar`, `title_en`, `desc_ar`, `desc_en`, `created_at`, `updated_at`) VALUES
(1, 'عن الموقع', 'About Us', NULL, NULL, NULL, NULL),
(2, 'الشروط والاحكام', 'Terms & Conditions', NULL, NULL, NULL, NULL),
(3, 'سياسة الخصوصية', 'Privacy policy', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'settings', 'settings', 'settings', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(2, 'updatesetting', 'updatesetting', 'updatesetting', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(3, 'updatesocial', 'updatesocial', 'updatesocial', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(4, 'updatelocation', 'updatelocation', 'updatelocation', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(5, 'updateseo', 'updateseo', 'updateseo', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(6, 'permissions', 'permissions', 'permissions', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(7, 'addpagepermission', 'addpagepermission', 'addpagepermission', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(8, 'addpermission', 'addpermission', 'addpermission', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(9, 'editpagepermission', 'editpagepermission', 'editpagepermission', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(10, 'updatepermission', 'updatepermission', 'updatepermission', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(11, 'deletepermission', 'deletepermission', 'deletepermission', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(12, 'admins', 'admins', 'admins', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(13, 'addadmin', 'addadmin', 'addadmin', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(14, 'updateadmin', 'updateadmin', 'updateadmin', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(15, 'deleteadmin', 'deleteadmin', 'deleteadmin', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(16, 'deleteadmins', 'deleteadmins', 'deleteadmins', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(17, 'users', 'users', 'users', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(18, 'adduser', 'adduser', 'adduser', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(19, 'updateuser', 'updateuser', 'updateuser', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(20, 'sendnotifyuser', 'sendnotifyuser', 'sendnotifyuser', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(21, 'deleteuser', 'deleteuser', 'deleteuser', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(22, 'deleteusers', 'deleteusers', 'deleteusers', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(23, 'changestatususer', 'changestatususer', 'changestatususer', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(24, 'providers', 'providers', 'providers', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(25, 'addprovider', 'addprovider', 'addprovider', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(26, 'updateprovider', 'updateprovider', 'updateprovider', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(27, 'sendnotifyprovider', 'sendnotifyprovider', 'sendnotifyprovider', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(28, 'deleteprovider', 'deleteprovider', 'deleteprovider', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(29, 'deleteproviders', 'deleteproviders', 'deleteproviders', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(30, 'changestatusprovider', 'changestatusprovider', 'changestatusprovider', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(31, 'pages', 'pages', 'pages', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(32, 'addpage', 'addpage', 'addpage', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(33, 'updatepage', 'updatepage', 'updatepage', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(34, 'deletepage', 'deletepage', 'deletepage', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(35, 'deletepages', 'deletepages', 'deletepages', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(36, 'sections', 'sections', 'sections', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(37, 'addsection', 'addsection', 'addsection', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(38, 'updatesection', 'updatesection', 'updatesection', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(39, 'deletesection', 'deletesection', 'deletesection', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(40, 'deletesections', 'deletesections', 'deletesections', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(41, 'services', 'services', 'services', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(42, 'addservice', 'addservice', 'addservice', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(43, 'updateservice', 'updateservice', 'updateservice', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(44, 'deleteservice', 'deleteservice', 'deleteservice', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(45, 'deleteservices', 'deleteservices', 'deleteservices', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(46, 'countrys', 'countrys', 'countrys', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(47, 'addcountry', 'addcountry', 'addcountry', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(48, 'updatecountry', 'updatecountry', 'updatecountry', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(49, 'deletecountry', 'deletecountry', 'deletecountry', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(50, 'deletecountrys', 'deletecountrys', 'deletecountrys', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(51, 'citys', 'citys', 'citys', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(52, 'addcity', 'addcity', 'addcity', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(53, 'updatecity', 'updatecity', 'updatecity', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(54, 'deletecity', 'deletecity', 'deletecity', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(55, 'deletecitys', 'deletecitys', 'deletecitys', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(56, 'neighborhoods', 'neighborhoods', 'neighborhoods', 1, '2020-03-13 10:48:15', '2020-03-13 10:48:15'),
(57, 'addneighborhood', 'addneighborhood', 'addneighborhood', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(58, 'updateneighborhood', 'updateneighborhood', 'updateneighborhood', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(59, 'deleteneighborhood', 'deleteneighborhood', 'deleteneighborhood', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(60, 'deleteneighborhoods', 'deleteneighborhoods', 'deleteneighborhoods', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(61, 'contacts', 'contacts', 'contacts', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(62, 'deletecontact', 'deletecontact', 'deletecontact', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(63, 'deletecontacts', 'deletecontacts', 'deletecontacts', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(64, 'adminReports', 'adminReports', 'adminReports', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(65, 'deleteadminReport', 'deleteadminReport', 'deleteadminReport', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(66, 'deleteadminReports', 'deleteadminReports', 'deleteadminReports', 1, '2020-03-13 10:48:16', '2020-03-13 10:48:16'),
(67, 'settings', 'settings', 'settings', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(68, 'updatesetting', 'updatesetting', 'updatesetting', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(69, 'updatesocial', 'updatesocial', 'updatesocial', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(70, 'updatelocation', 'updatelocation', 'updatelocation', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(71, 'updateseo', 'updateseo', 'updateseo', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(72, 'users', 'users', 'users', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(73, 'adduser', 'adduser', 'adduser', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(74, 'updateuser', 'updateuser', 'updateuser', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(75, 'sendnotifyuser', 'sendnotifyuser', 'sendnotifyuser', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(76, 'deleteuser', 'deleteuser', 'deleteuser', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(77, 'deleteusers', 'deleteusers', 'deleteusers', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(78, 'changestatususer', 'changestatususer', 'changestatususer', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(79, 'providers', 'providers', 'providers', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(80, 'addprovider', 'addprovider', 'addprovider', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(81, 'updateprovider', 'updateprovider', 'updateprovider', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(82, 'sendnotifyprovider', 'sendnotifyprovider', 'sendnotifyprovider', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(83, 'deleteprovider', 'deleteprovider', 'deleteprovider', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(84, 'deleteproviders', 'deleteproviders', 'deleteproviders', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(85, 'changestatusprovider', 'changestatusprovider', 'changestatusprovider', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(86, 'pages', 'pages', 'pages', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(87, 'addpage', 'addpage', 'addpage', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(88, 'updatepage', 'updatepage', 'updatepage', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(89, 'deletepage', 'deletepage', 'deletepage', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(90, 'deletepages', 'deletepages', 'deletepages', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(91, 'sections', 'sections', 'sections', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(92, 'addsection', 'addsection', 'addsection', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(93, 'updatesection', 'updatesection', 'updatesection', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(94, 'deletesection', 'deletesection', 'deletesection', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(95, 'deletesections', 'deletesections', 'deletesections', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(96, 'services', 'services', 'services', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(97, 'addservice', 'addservice', 'addservice', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(98, 'updateservice', 'updateservice', 'updateservice', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(99, 'deleteservice', 'deleteservice', 'deleteservice', 2, '2020-03-13 10:48:30', '2020-03-13 10:48:30'),
(100, 'deleteservices', 'deleteservices', 'deleteservices', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(101, 'countrys', 'countrys', 'countrys', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(102, 'addcountry', 'addcountry', 'addcountry', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(103, 'updatecountry', 'updatecountry', 'updatecountry', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(104, 'deletecountry', 'deletecountry', 'deletecountry', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(105, 'deletecountrys', 'deletecountrys', 'deletecountrys', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(106, 'citys', 'citys', 'citys', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(107, 'addcity', 'addcity', 'addcity', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(108, 'updatecity', 'updatecity', 'updatecity', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(109, 'deletecity', 'deletecity', 'deletecity', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(110, 'deletecitys', 'deletecitys', 'deletecitys', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(111, 'neighborhoods', 'neighborhoods', 'neighborhoods', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(112, 'addneighborhood', 'addneighborhood', 'addneighborhood', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(113, 'updateneighborhood', 'updateneighborhood', 'updateneighborhood', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(114, 'deleteneighborhood', 'deleteneighborhood', 'deleteneighborhood', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(115, 'deleteneighborhoods', 'deleteneighborhoods', 'deleteneighborhoods', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(116, 'contacts', 'contacts', 'contacts', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(117, 'deletecontact', 'deletecontact', 'deletecontact', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31'),
(118, 'deletecontacts', 'deletecontacts', 'deletecontacts', 2, '2020-03-13 10:48:31', '2020-03-13 10:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'Super Admin', 'Super Admin', '2020-03-13 10:36:31', '2020-03-13 10:36:31'),
(2, 'Admin', 'Admin', 'Admin', '2020-03-13 10:48:30', '2020-03-13 10:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_desc_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_desc_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title_ar`, `title_en`, `short_desc_ar`, `short_desc_en`, `desc_ar`, `desc_en`, `image`, `created_at`, `updated_at`) VALUES
(1, 'مطاعم', 'Restaurants', 'مطاعم مطاعم', 'Restaurants Restaurants', 'مطاعم مطاعم مطاعم مطاعم', 'Restaurants Restaurants Restaurants Restaurants', '/public/images/sections/13-03-2015841045091799058891.jpg', '2020-03-13 11:01:49', '2020-03-13 11:01:49'),
(2, 'كافيهات', 'Cafes', 'كافيهات كافيهات', 'Cafes Cafes', 'كافيهات كافيهات كافيهات كافيهات', 'Cafes Cafes Cafes Cafes', '/public/images/sections/13-03-2015841045711238087814.jpg', '2020-03-13 11:02:51', '2020-03-13 11:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_desc_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_desc_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title_ar`, `title_en`, `short_desc_ar`, `short_desc_en`, `desc_ar`, `desc_en`, `amount`, `price`, `image`, `section_id`, `created_at`, `updated_at`) VALUES
(1, 'برجر كينج', 'Burger King', 'برجر كينج برجر كينج', 'Burger King Burger King', 'برجر كينج برجر كينج برجر كينج برجر كينج', 'Burger King Burger King Burger King Burger King', NULL, NULL, '/public/images/services/13-03-2015841046771357319964.png', 1, '2020-03-13 11:04:37', '2020-03-13 11:04:37'),
(2, 'كوستا', 'Costa', 'كوستا كوستا', 'Costa Costa', 'كوستا كوستا كوستا كوستا', 'Costa Costa Costa Costa', NULL, NULL, '/public/images/services/13-03-2015841047151017769597.png', 2, '2020-03-13 11:05:15', '2020-03-13 11:05:15'),
(3, 'كنتاكي', 'KFC', 'كنتاكي كنتاكي', 'KFC KFC', 'كنتاكي كنتاكي كنتاكي كنتاكي', 'KFC KFC KFC KFC', NULL, NULL, '/public/images/services/13-03-201584104758158664225.png', 1, '2020-03-13 11:05:58', '2020-03-13 11:05:58'),
(4, 'ستاربكس', 'Starbucks', 'ستاربكس ستاربكس', 'Starbucks Starbucks', 'ستاربكس ستاربكس ستاربكس ستاربكس', 'Starbucks Starbucks Starbucks Starbucks', NULL, NULL, '/public/images/services/13-03-201584104833847212895.png', 2, '2020-03-13 11:07:13', '2020-03-13 11:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'DashBoard', '2020-03-13 10:36:52', '2020-03-13 10:42:05'),
(2, 'email', 'info@site.sa', '2020-03-13 10:41:45', '2020-03-13 10:42:05'),
(3, 'phone', '966123456789', '2020-03-13 10:41:45', '2020-03-13 10:42:05'),
(4, 'facebook', 'https://www.facebook.com', '2020-03-13 10:41:45', '2020-03-13 10:42:33'),
(5, 'twitter', 'https://www.twitter.com', '2020-03-13 10:41:45', '2020-03-13 10:42:33'),
(6, 'instagram', 'https://www.instagram.com', '2020-03-13 10:41:45', '2020-03-13 10:42:33'),
(7, 'snapchat', 'site-snapchat', '2020-03-13 10:41:45', '2020-03-13 10:42:33'),
(8, 'lat', '30.0444196', '2020-03-13 10:41:45', '2020-03-13 10:47:38'),
(9, 'lng', '31.2357116', '2020-03-13 10:41:45', '2020-03-13 10:47:38'),
(10, 'description', 'site and app', '2020-03-13 10:41:45', '2020-03-13 10:43:24'),
(11, 'key_words', 'site , app', '2020-03-13 10:41:45', '2020-03-13 10:43:24'),
(12, 'mobile', NULL, '2020-03-13 10:42:05', '2020-03-13 10:42:05'),
(13, 'benfit_percentage', NULL, '2020-03-13 10:42:05', '2020-03-13 10:42:05'),
(14, 'address', NULL, '2020-03-13 10:42:05', '2020-03-13 10:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `compelete` tinyint(1) DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neighborhood_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `code`, `user_type`, `active`, `blocked`, `compelete`, `avatar`, `lat`, `lng`, `address`, `country_id`, `city_id`, `neighborhood_id`, `role_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'المدير العام', 'admin@info.com', '0541867992', NULL, 'admin', 1, 0, NULL, 'public/user.png', NULL, NULL, 'السعودية - الرياض', NULL, NULL, NULL, 1, NULL, '$2y$10$GfvkmJx10IzEngkdbCUFA.i5FeSC2ef59DKVf.p72PyTABO2ndmBC', NULL, '2020-03-13 10:36:32', '2020-03-13 10:36:32'),
(2, 'احمد', 'admin2@info.com', '966123456780', NULL, 'admin', NULL, NULL, NULL, '/public/images/admins/13-03-201584103771721669651.png', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '$2y$10$rVXebjmaXYfGTDDYTstR.eHnUHtkgKi0Pz42/WErcOpBzXgZ4ld5K', NULL, '2020-03-13 10:49:31', '2020-03-13 10:49:31'),
(3, 'عبدالرحمن', 'abdo@gmail.com', '201061656278', NULL, 'client', NULL, NULL, NULL, '/public/images/users/13-03-2015841038311175322600.png', NULL, NULL, 'السعودية - الرياض', NULL, NULL, NULL, 1, NULL, '$2y$10$hhCmZ/JrA19o9riz9dsUh.Raj2SZJGh5z1R7aLsOSI9b.7.Q7gcI6', NULL, '2020-03-13 10:50:31', '2020-03-13 10:50:31'),
(4, 'محمد', 'mohamed@gmail.com', '201061656277', NULL, 'client', NULL, NULL, NULL, '/public/images/users/13-03-2015841038771428229134.png', NULL, NULL, 'السعودية - المدينة', NULL, NULL, NULL, 1, NULL, '$2y$10$bFi2UArARGGU/1ijqjgxzu5c6y1rkh8.1R0t5iERzAUURjOQxUpqO', NULL, '2020-03-13 10:51:17', '2020-03-13 10:51:17'),
(5, 'يوسف', 'jo@gmail.com', '201061656276', NULL, 'client', NULL, NULL, NULL, '/public/images/users/13-03-201584103917866003347.png', NULL, NULL, 'السعودية - جدة', NULL, NULL, NULL, 1, NULL, '$2y$10$Afmjg41Wqxvx2bD432pm9OP3KhTL0l.zY71N3WhPC/Jj5cRo11wuq', NULL, '2020-03-13 10:51:57', '2020-03-13 10:51:57'),
(6, 'ابراهيم', 'hima@gmail.com', '201061656275', NULL, 'client', NULL, NULL, NULL, '/public/images/users/13-03-201584103987764350293.png', NULL, NULL, 'السعودية - مكة', NULL, NULL, NULL, 1, NULL, '$2y$10$6/DzydZvBrhTBw5flwRcuOj5GN/hNrvlacghHxO2rFJnacoKQkQcC', NULL, '2020-03-13 10:53:07', '2020-03-13 10:53:07'),
(7, 'محمود', 'mahmoud@gmail.com', '201061656274', NULL, 'provider', NULL, NULL, NULL, '/public/images/users/13-03-2015841040411209704768.png', NULL, NULL, 'السعودية - المدينة', NULL, NULL, NULL, 1, NULL, '$2y$10$zlbKIGvC1fAzwbxoYXBWFu0gmXzIOASufkc4fFIp1XG4GyWMUd9gi', NULL, '2020-03-13 10:54:01', '2020-03-13 10:54:01'),
(8, 'سمير', 'samir@gmail.com', '201061656273', NULL, 'provider', NULL, NULL, NULL, '/public/images/users/13-03-201584104103197443830.png', NULL, NULL, 'السعودية - جدة', NULL, NULL, NULL, 1, NULL, '$2y$10$FrK3sAZKqAZVuIFyDVZdneDY9Y7EimIPRI1afwSuioxmdHnusQfne', NULL, '2020-03-13 10:55:03', '2020-03-13 10:55:03'),
(9, 'حمدي', 'hamdy@gmail.com', '201061656272', NULL, 'provider', NULL, NULL, NULL, '/public/images/users/13-03-201584104161114357210.png', NULL, NULL, 'السعودية - مكة', NULL, NULL, NULL, 1, NULL, '$2y$10$bQcpYEUYg2IxVP.a0qOjtOyTsN5K8vUEkxVB31QanOoK.verm0HLS', NULL, '2020-03-13 10:56:01', '2020-03-13 10:56:01'),
(10, 'عبد الله', 'body@gmail.com', '201061656271', NULL, 'provider', NULL, NULL, NULL, '/public/images/users/13-03-201584104243807845264.png', NULL, NULL, 'السعودية - الرياض', NULL, NULL, NULL, 1, NULL, '$2y$10$j79PWYzlQAUcnwUP2jU2.eSAArTbWUzE6u4ChXUu2QcU1cht9SsHK', NULL, '2020-03-13 10:57:23', '2020-03-13 10:57:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_reports`
--
ALTER TABLE `admin_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contacts_email_unique` (`email`),
  ADD UNIQUE KEY `contacts_phone_unique` (`phone`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `neighborhoods_country_id_foreign` (`country_id`),
  ADD KEY `neighborhoods_city_id_foreign` (`city_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_section_id_foreign` (`section_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_reports`
--
ALTER TABLE `admin_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD CONSTRAINT `neighborhoods_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `neighborhoods_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
